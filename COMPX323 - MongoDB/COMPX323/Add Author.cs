﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Oracle.ManagedDataAccess.Client;

namespace COMPX323
{
    public partial class Add_Author : Form
    {
        public Add_Author()
        {
            InitializeComponent();
        }

        private void buttonAback_Click(object sender, EventArgs e)
        {
            Hide();
            Add add = new Add();
            add.ShowDialog();
        }

        private void labelAname_Click(object sender, EventArgs e)
        {

        }

        private void Add_Author_Load(object sender, EventArgs e)
        {
    
        }

        private void buttonAddAuthor_Click(object sender, EventArgs e)
        {
            DialogResult result = MessageBox.Show("Do you want to add this author to the database?", "Confirmation", MessageBoxButtons.YesNoCancel);
            if (result == DialogResult.Yes)
            {
                string oradb = "Data Source=oracle.cms.waikato.ac.nz:1521/teaching.cms.waikato.ac.nz;;User Id=COMPX323_08;Password=Hsp369G2FU;";
                OracleConnection conn = new OracleConnection(oradb);  // C#
                conn.Open();
                OracleCommand cmd = new OracleCommand();
                cmd.Connection = conn;
                cmd.CommandText = $@"INSERT INTO author (author_id, qualification, name) VALUES (author_sequence.NEXTVAL, '{textBoxQualification.Text}', '{textBoxAFname.Text}')";
                cmd.ExecuteNonQuery();
                conn.Dispose();

            }
            else if (result == DialogResult.No)
            {

            }

            Hide();
            Add add = new Add();
            add.ShowDialog();
        }
    }
}
