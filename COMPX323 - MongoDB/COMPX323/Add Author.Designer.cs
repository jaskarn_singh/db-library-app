﻿namespace COMPX323
{
    partial class Add_Author
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Add_Author));
            this.labelAname = new System.Windows.Forms.Label();
            this.labelQualification = new System.Windows.Forms.Label();
            this.textBoxAFname = new System.Windows.Forms.TextBox();
            this.textBoxQualification = new System.Windows.Forms.TextBox();
            this.buttonAback = new System.Windows.Forms.Button();
            this.buttonAddAuthor = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // labelAname
            // 
            resources.ApplyResources(this.labelAname, "labelAname");
            this.labelAname.Name = "labelAname";
            this.labelAname.Click += new System.EventHandler(this.labelAname_Click);
            // 
            // labelQualification
            // 
            resources.ApplyResources(this.labelQualification, "labelQualification");
            this.labelQualification.Name = "labelQualification";
            // 
            // textBoxAFname
            // 
            resources.ApplyResources(this.textBoxAFname, "textBoxAFname");
            this.textBoxAFname.Name = "textBoxAFname";
            // 
            // textBoxQualification
            // 
            resources.ApplyResources(this.textBoxQualification, "textBoxQualification");
            this.textBoxQualification.Name = "textBoxQualification";
            // 
            // buttonAback
            // 
            resources.ApplyResources(this.buttonAback, "buttonAback");
            this.buttonAback.Name = "buttonAback";
            this.buttonAback.UseVisualStyleBackColor = true;
            this.buttonAback.Click += new System.EventHandler(this.buttonAback_Click);
            // 
            // buttonAddAuthor
            // 
            resources.ApplyResources(this.buttonAddAuthor, "buttonAddAuthor");
            this.buttonAddAuthor.Name = "buttonAddAuthor";
            this.buttonAddAuthor.UseVisualStyleBackColor = true;
            this.buttonAddAuthor.Click += new System.EventHandler(this.buttonAddAuthor_Click);
            // 
            // Add_Author
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.Controls.Add(this.buttonAddAuthor);
            this.Controls.Add(this.buttonAback);
            this.Controls.Add(this.textBoxQualification);
            this.Controls.Add(this.textBoxAFname);
            this.Controls.Add(this.labelQualification);
            this.Controls.Add(this.labelAname);
            this.Name = "Add_Author";
            this.ShowIcon = false;
            this.Load += new System.EventHandler(this.Add_Author_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label labelAname;
        private System.Windows.Forms.Label labelQualification;
        private System.Windows.Forms.TextBox textBoxAFname;
        private System.Windows.Forms.TextBox textBoxQualification;
        private System.Windows.Forms.Button buttonAback;
        private System.Windows.Forms.Button buttonAddAuthor;
    }
}