﻿namespace COMPX323
{
    partial class Issue
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.labelissueMemId = new System.Windows.Forms.Label();
            this.labelIssueBookID = new System.Windows.Forms.Label();
            this.textBoximemID = new System.Windows.Forms.TextBox();
            this.textBoxiBookID = new System.Windows.Forms.TextBox();
            this.buttonIssue = new System.Windows.Forms.Button();
            this.buttoniBack = new System.Windows.Forms.Button();
            this.labelissueDate = new System.Windows.Forms.Label();
            this.labeliReturndate = new System.Windows.Forms.Label();
            this.dateTimePicker1 = new System.Windows.Forms.DateTimePicker();
            this.dateTimePicker2 = new System.Windows.Forms.DateTimePicker();
            this.SuspendLayout();
            // 
            // labelissueMemId
            // 
            this.labelissueMemId.AutoSize = true;
            this.labelissueMemId.Location = new System.Drawing.Point(97, 107);
            this.labelissueMemId.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.labelissueMemId.Name = "labelissueMemId";
            this.labelissueMemId.Size = new System.Drawing.Size(59, 13);
            this.labelissueMemId.TabIndex = 0;
            this.labelissueMemId.Text = "Member ID";
            // 
            // labelIssueBookID
            // 
            this.labelIssueBookID.AutoSize = true;
            this.labelIssueBookID.Location = new System.Drawing.Point(111, 128);
            this.labelIssueBookID.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.labelIssueBookID.Name = "labelIssueBookID";
            this.labelIssueBookID.Size = new System.Drawing.Size(46, 13);
            this.labelIssueBookID.TabIndex = 1;
            this.labelIssueBookID.Text = "Book ID";
            // 
            // textBoximemID
            // 
            this.textBoximemID.Location = new System.Drawing.Point(159, 105);
            this.textBoximemID.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.textBoximemID.Name = "textBoximemID";
            this.textBoximemID.Size = new System.Drawing.Size(135, 20);
            this.textBoximemID.TabIndex = 2;
            // 
            // textBoxiBookID
            // 
            this.textBoxiBookID.Location = new System.Drawing.Point(159, 126);
            this.textBoxiBookID.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.textBoxiBookID.Name = "textBoxiBookID";
            this.textBoxiBookID.Size = new System.Drawing.Size(135, 20);
            this.textBoxiBookID.TabIndex = 3;
            // 
            // buttonIssue
            // 
            this.buttonIssue.Location = new System.Drawing.Point(90, 229);
            this.buttonIssue.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.buttonIssue.Name = "buttonIssue";
            this.buttonIssue.Size = new System.Drawing.Size(65, 24);
            this.buttonIssue.TabIndex = 4;
            this.buttonIssue.Text = "Issue";
            this.buttonIssue.UseVisualStyleBackColor = true;
            this.buttonIssue.Click += new System.EventHandler(this.buttonIssue_Click);
            // 
            // buttoniBack
            // 
            this.buttoniBack.Location = new System.Drawing.Point(246, 229);
            this.buttoniBack.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.buttoniBack.Name = "buttoniBack";
            this.buttoniBack.Size = new System.Drawing.Size(65, 24);
            this.buttoniBack.TabIndex = 5;
            this.buttoniBack.Text = "Back";
            this.buttoniBack.UseVisualStyleBackColor = true;
            this.buttoniBack.Click += new System.EventHandler(this.buttoniBack_Click);
            // 
            // labelissueDate
            // 
            this.labelissueDate.AutoSize = true;
            this.labelissueDate.Location = new System.Drawing.Point(97, 150);
            this.labelissueDate.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.labelissueDate.Name = "labelissueDate";
            this.labelissueDate.Size = new System.Drawing.Size(58, 13);
            this.labelissueDate.TabIndex = 6;
            this.labelissueDate.Text = "Issue Date";
            // 
            // labeliReturndate
            // 
            this.labeliReturndate.AutoSize = true;
            this.labeliReturndate.Location = new System.Drawing.Point(91, 171);
            this.labeliReturndate.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.labeliReturndate.Name = "labeliReturndate";
            this.labeliReturndate.Size = new System.Drawing.Size(65, 13);
            this.labeliReturndate.TabIndex = 9;
            this.labeliReturndate.Text = "Return Date";
            // 
            // dateTimePicker1
            // 
            this.dateTimePicker1.Location = new System.Drawing.Point(159, 147);
            this.dateTimePicker1.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.dateTimePicker1.Name = "dateTimePicker1";
            this.dateTimePicker1.Size = new System.Drawing.Size(135, 20);
            this.dateTimePicker1.TabIndex = 10;
            // 
            // dateTimePicker2
            // 
            this.dateTimePicker2.Location = new System.Drawing.Point(159, 168);
            this.dateTimePicker2.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.dateTimePicker2.Name = "dateTimePicker2";
            this.dateTimePicker2.Size = new System.Drawing.Size(135, 20);
            this.dateTimePicker2.TabIndex = 11;
            // 
            // Issue
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(381, 328);
            this.Controls.Add(this.dateTimePicker2);
            this.Controls.Add(this.dateTimePicker1);
            this.Controls.Add(this.labeliReturndate);
            this.Controls.Add(this.labelissueDate);
            this.Controls.Add(this.buttoniBack);
            this.Controls.Add(this.buttonIssue);
            this.Controls.Add(this.textBoxiBookID);
            this.Controls.Add(this.textBoximemID);
            this.Controls.Add(this.labelIssueBookID);
            this.Controls.Add(this.labelissueMemId);
            this.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.Name = "Issue";
            this.Text = "Issue";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label labelissueMemId;
        private System.Windows.Forms.Label labelIssueBookID;
        private System.Windows.Forms.TextBox textBoximemID;
        private System.Windows.Forms.TextBox textBoxiBookID;
        private System.Windows.Forms.Button buttonIssue;
        private System.Windows.Forms.Button buttoniBack;
        private System.Windows.Forms.Label labelissueDate;
        private System.Windows.Forms.Label labeliReturndate;
        private System.Windows.Forms.DateTimePicker dateTimePicker1;
        private System.Windows.Forms.DateTimePicker dateTimePicker2;
    }
}