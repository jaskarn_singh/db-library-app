﻿namespace COMPX323
{
    partial class mongo_add
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.bookname = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.authName = new System.Windows.Forms.TextBox();
            this.pubName = new System.Windows.Forms.TextBox();
            this.suppliername = new System.Windows.Forms.TextBox();
            this.supplieremail = new System.Windows.Forms.TextBox();
            this.authQual = new System.Windows.Forms.TextBox();
            this.pubEmail = new System.Windows.Forms.TextBox();
            this.pubPhone = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(96, 62);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(61, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Book name";
            this.label1.Click += new System.EventHandler(this.label1_Click);
            // 
            // bookname
            // 
            this.bookname.Location = new System.Drawing.Point(163, 59);
            this.bookname.Name = "bookname";
            this.bookname.Size = new System.Drawing.Size(121, 20);
            this.bookname.TabIndex = 1;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(80, 89);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(77, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Book Category";
            // 
            // comboBox1
            // 
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Location = new System.Drawing.Point(163, 89);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(121, 21);
            this.comboBox1.TabIndex = 3;
            // 
            // authName
            // 
            this.authName.Location = new System.Drawing.Point(163, 197);
            this.authName.Name = "authName";
            this.authName.Size = new System.Drawing.Size(121, 20);
            this.authName.TabIndex = 7;
            // 
            // pubName
            // 
            this.pubName.Location = new System.Drawing.Point(163, 223);
            this.pubName.Name = "pubName";
            this.pubName.Size = new System.Drawing.Size(121, 20);
            this.pubName.TabIndex = 8;
            // 
            // suppliername
            // 
            this.suppliername.Location = new System.Drawing.Point(163, 116);
            this.suppliername.Name = "suppliername";
            this.suppliername.Size = new System.Drawing.Size(121, 20);
            this.suppliername.TabIndex = 9;
            // 
            // supplieremail
            // 
            this.supplieremail.Location = new System.Drawing.Point(163, 142);
            this.supplieremail.Name = "supplieremail";
            this.supplieremail.Size = new System.Drawing.Size(121, 20);
            this.supplieremail.TabIndex = 10;
            // 
            // authQual
            // 
            this.authQual.Location = new System.Drawing.Point(163, 168);
            this.authQual.Name = "authQual";
            this.authQual.Size = new System.Drawing.Size(121, 20);
            this.authQual.TabIndex = 11;
            // 
            // pubEmail
            // 
            this.pubEmail.Location = new System.Drawing.Point(163, 249);
            this.pubEmail.Name = "pubEmail";
            this.pubEmail.Size = new System.Drawing.Size(121, 20);
            this.pubEmail.TabIndex = 12;
            // 
            // pubPhone
            // 
            this.pubPhone.Location = new System.Drawing.Point(163, 275);
            this.pubPhone.Name = "pubPhone";
            this.pubPhone.Size = new System.Drawing.Size(121, 20);
            this.pubPhone.TabIndex = 13;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(83, 119);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(74, 13);
            this.label3.TabIndex = 14;
            this.label3.Text = "Supplier name";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(85, 145);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(72, 13);
            this.label4.TabIndex = 15;
            this.label4.Text = "Supplier email";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(58, 171);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(99, 13);
            this.label5.TabIndex = 16;
            this.label5.Text = "Author Qualification";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(90, 200);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(67, 13);
            this.label6.TabIndex = 17;
            this.label6.Text = "Author name";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(78, 226);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(79, 13);
            this.label7.TabIndex = 18;
            this.label7.Text = "Publisher name";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(80, 252);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(77, 13);
            this.label8.TabIndex = 19;
            this.label8.Text = "Publisher email";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(74, 278);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(83, 13);
            this.label9.TabIndex = 20;
            this.label9.Text = "Publisher phone";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(99, 312);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 21;
            this.button1.Text = "Add";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(198, 312);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(75, 23);
            this.button2.TabIndex = 22;
            this.button2.Text = "Back";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // mongo_add
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(363, 358);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.pubPhone);
            this.Controls.Add(this.pubEmail);
            this.Controls.Add(this.authQual);
            this.Controls.Add(this.supplieremail);
            this.Controls.Add(this.suppliername);
            this.Controls.Add(this.pubName);
            this.Controls.Add(this.authName);
            this.Controls.Add(this.comboBox1);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.bookname);
            this.Controls.Add(this.label1);
            this.Name = "mongo_add";
            this.Text = "mongo_add";
            this.Load += new System.EventHandler(this.mongo_add_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox bookname;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.TextBox authName;
        private System.Windows.Forms.TextBox pubName;
        private System.Windows.Forms.TextBox suppliername;
        private System.Windows.Forms.TextBox supplieremail;
        private System.Windows.Forms.TextBox authQual;
        private System.Windows.Forms.TextBox pubEmail;
        private System.Windows.Forms.TextBox pubPhone;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
    }
}