﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Oracle.ManagedDataAccess.Client;
using MongoDB.Bson;
using MongoDB.Driver;

namespace COMPX323
{
    public partial class Add_Book : Form
    {
        public Add_Book()
        {
            InitializeComponent();
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void buttonBback_Click(object sender, EventArgs e)
        {
            Hide();
            Add add = new Add();
            add.ShowDialog();
        }

        private void buttonAddBook_Click(object sender, EventArgs e)
        {
            DialogResult result = MessageBox.Show("Do you want to add this book to the database?", "Confirmation", MessageBoxButtons.YesNoCancel);
            if (result == DialogResult.Yes)
            {
                string oradb = "Data Source=oracle.cms.waikato.ac.nz:1521/teaching.cms.waikato.ac.nz;;User Id=COMPX323_08;Password=Hsp369G2FU;";
                OracleConnection conn = new OracleConnection(oradb);  // C#
                conn.Open();
                OracleCommand cmd = new OracleCommand();
                cmd.Connection = conn;
                cmd.CommandText = $@"INSERT INTO books (book_id, bookname, bookcategory) VALUES(books_sequence.NEXTVAL, '{textBox1.Text}', '{comboBox4.Text}')";
                cmd.ExecuteNonQuery();

                cmd.CommandText = $@"INSERT INTO published (book_id, pub_id) select b.book_id, p.pub_id from books b, publisher p where b.bookname = '{textBox1.Text}' and p.name = '{comboBox1.Text}' ";
                cmd.ExecuteNonQuery();

                cmd.CommandText = $@"INSERT INTO supplied (book_id, sup_id) select b.book_id, s.sup_id from books b, supplier s where b.bookname = '{textBox1.Text}' and s.name = '{comboBox2.Text}' ";
                cmd.ExecuteNonQuery();

                cmd.CommandText = $@"INSERT INTO written (book_id, author_id) select b.book_id, a.author_id from books b, author a where b.bookname = '{textBox1.Text}' and a.name = '{comboBox3.Text}' ";
                cmd.ExecuteNonQuery();

                conn.Dispose();
            }
            else if (result == DialogResult.No)
            {

            }
            

            Hide();
            Add add = new Add();
            add.ShowDialog();
        }

        private void comboBox4_SelectedIndexChanged(object sender, EventArgs e)
        {
         
        }

        private void Add_Book_Load(object sender, EventArgs e)
        {
            comboBox4.Items.Add("Sci Fi");
            comboBox4.Items.Add("Fantasy");
            comboBox4.Items.Add("Post Apocalyptic");
            comboBox4.Items.Add("Drama");
            comboBox4.Items.Add("Comedy");

            string oradb = "Data Source=oracle.cms.waikato.ac.nz:1521/teaching.cms.waikato.ac.nz;;User Id=COMPX323_08;Password=Hsp369G2FU;";
            OracleConnection conn = new OracleConnection(oradb);  // C#
            conn.Open();
            OracleCommand cmd = new OracleCommand();
            cmd.Connection = conn;
            cmd.CommandText = "select a.name from author a";
            cmd.CommandType = CommandType.Text;
            OracleDataReader dr = cmd.ExecuteReader();
            while (dr.Read())
            {
                comboBox3.Items.Add(dr[0]);

            }



            cmd.Connection = conn;
            cmd.CommandText = "select p.name from publisher p";
            cmd.CommandType = CommandType.Text;
            OracleDataReader dr1 = cmd.ExecuteReader();
            while (dr1.Read())
            {
                comboBox1.Items.Add(dr1[0]);
            }



            cmd.Connection = conn;
            cmd.CommandText = "select s.name from supplier s";
            cmd.CommandType = CommandType.Text;
            OracleDataReader dr2 = cmd.ExecuteReader();
            while (dr2.Read())
            {
                comboBox2.Items.Add(dr2[0]);
            }

            conn.Dispose();

        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
    }
}
