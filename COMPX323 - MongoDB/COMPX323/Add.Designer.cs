﻿namespace COMPX323
{
    partial class Add
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.buttonAddBook = new System.Windows.Forms.Button();
            this.buttonAddAuthor = new System.Windows.Forms.Button();
            this.buttonAddMember = new System.Windows.Forms.Button();
            this.buttonAddPublisher = new System.Windows.Forms.Button();
            this.buttonAddSupplier = new System.Windows.Forms.Button();
            this.buttonaddBack = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // buttonAddBook
            // 
            this.buttonAddBook.Location = new System.Drawing.Point(79, 56);
            this.buttonAddBook.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.buttonAddBook.Name = "buttonAddBook";
            this.buttonAddBook.Size = new System.Drawing.Size(87, 44);
            this.buttonAddBook.TabIndex = 0;
            this.buttonAddBook.Text = "Add Book";
            this.buttonAddBook.UseVisualStyleBackColor = true;
            this.buttonAddBook.Click += new System.EventHandler(this.button1_Click);
            // 
            // buttonAddAuthor
            // 
            this.buttonAddAuthor.Location = new System.Drawing.Point(79, 150);
            this.buttonAddAuthor.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.buttonAddAuthor.Name = "buttonAddAuthor";
            this.buttonAddAuthor.Size = new System.Drawing.Size(87, 44);
            this.buttonAddAuthor.TabIndex = 1;
            this.buttonAddAuthor.Text = "Add Author";
            this.buttonAddAuthor.UseVisualStyleBackColor = true;
            this.buttonAddAuthor.Click += new System.EventHandler(this.buttonAddAuthor_Click);
            // 
            // buttonAddMember
            // 
            this.buttonAddMember.Location = new System.Drawing.Point(227, 56);
            this.buttonAddMember.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.buttonAddMember.Name = "buttonAddMember";
            this.buttonAddMember.Size = new System.Drawing.Size(87, 44);
            this.buttonAddMember.TabIndex = 2;
            this.buttonAddMember.Text = "Add Member";
            this.buttonAddMember.UseVisualStyleBackColor = true;
            this.buttonAddMember.Click += new System.EventHandler(this.buttonAddMember_Click);
            // 
            // buttonAddPublisher
            // 
            this.buttonAddPublisher.Location = new System.Drawing.Point(227, 150);
            this.buttonAddPublisher.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.buttonAddPublisher.Name = "buttonAddPublisher";
            this.buttonAddPublisher.Size = new System.Drawing.Size(87, 44);
            this.buttonAddPublisher.TabIndex = 3;
            this.buttonAddPublisher.Text = "Add Publisher";
            this.buttonAddPublisher.UseVisualStyleBackColor = true;
            this.buttonAddPublisher.Click += new System.EventHandler(this.buttonAddPublisher_Click);
            // 
            // buttonAddSupplier
            // 
            this.buttonAddSupplier.Location = new System.Drawing.Point(79, 233);
            this.buttonAddSupplier.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.buttonAddSupplier.Name = "buttonAddSupplier";
            this.buttonAddSupplier.Size = new System.Drawing.Size(87, 44);
            this.buttonAddSupplier.TabIndex = 4;
            this.buttonAddSupplier.Text = "Add Supplier";
            this.buttonAddSupplier.UseVisualStyleBackColor = true;
            this.buttonAddSupplier.Click += new System.EventHandler(this.buttonAddSupplier_Click);
            // 
            // buttonaddBack
            // 
            this.buttonaddBack.Location = new System.Drawing.Point(227, 233);
            this.buttonaddBack.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.buttonaddBack.Name = "buttonaddBack";
            this.buttonaddBack.Size = new System.Drawing.Size(87, 44);
            this.buttonaddBack.TabIndex = 5;
            this.buttonaddBack.Text = "Back";
            this.buttonaddBack.UseVisualStyleBackColor = true;
            this.buttonaddBack.Click += new System.EventHandler(this.buttonaddBack_Click);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(155, 104);
            this.button1.Margin = new System.Windows.Forms.Padding(2);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(87, 44);
            this.button1.TabIndex = 6;
            this.button1.Text = "Add (Mongo)";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click_1);
            // 
            // Add
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(396, 325);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.buttonaddBack);
            this.Controls.Add(this.buttonAddSupplier);
            this.Controls.Add(this.buttonAddPublisher);
            this.Controls.Add(this.buttonAddMember);
            this.Controls.Add(this.buttonAddAuthor);
            this.Controls.Add(this.buttonAddBook);
            this.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.Name = "Add";
            this.Text = "Add";
            this.Load += new System.EventHandler(this.Add_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button buttonAddBook;
        private System.Windows.Forms.Button buttonAddAuthor;
        private System.Windows.Forms.Button buttonAddMember;
        private System.Windows.Forms.Button buttonAddPublisher;
        private System.Windows.Forms.Button buttonAddSupplier;
        private System.Windows.Forms.Button buttonaddBack;
        private System.Windows.Forms.Button button1;
    }
}