﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MongoDB.Bson;
using MongoDB.Driver;

namespace COMPX323
{
    public partial class mongo_add : Form
    {
        public mongo_add()
        {
            InitializeComponent();
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void mongo_add_Load(object sender, EventArgs e)
        {
            comboBox1.Items.Add("Sci Fi");
            comboBox1.Items.Add("Fantasy");
            comboBox1.Items.Add("Post Apocalyptic");
            comboBox1.Items.Add("Drama");
            comboBox1.Items.Add("Comedy");
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Hide();
            Add add = new Add();
            add.ShowDialog();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            DialogResult result = MessageBox.Show("Do you want to add this book to the database?", "Confirmation", MessageBoxButtons.YesNoCancel);
            if (result == DialogResult.Yes)
            {
                MongoClient dbClient = new MongoClient("mongodb://compx323-08:X2GF57anb9p4hSSUY5h6@mongodb.cms.waikato.ac.nz:27017/compx323?authsource=admin");

                var db = dbClient.GetDatabase("compx323-08");
                var collection = db.GetCollection<BsonDocument>("libdb");

                var document = new BsonDocument
               {
                   {"bookname", bookname.Text},
                   {"bookcategory", comboBox1.Text},
                   {"supname", suppliername.Text},
                   {"supemail", supplieremail.Text},
                   {"supaddress", ""},
                   {"qualification", authQual.Text},
                   {"authorname", authName.Text},
                   {"pubname", pubName.Text},
                   {"pubemail", pubEmail.Text},
                   {"pubphone", pubPhone.Text},
                   {"pubaddress", ""}

               };
                collection.InsertOneAsync(document);

            }
            else if (result == DialogResult.No)
            {

            }


            Hide();
            Add add = new Add();
            add.ShowDialog();
        }
    }
}
