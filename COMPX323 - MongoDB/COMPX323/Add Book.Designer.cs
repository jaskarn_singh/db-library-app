﻿namespace COMPX323
{
    partial class Add_Book
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.labelBookName = new System.Windows.Forms.Label();
            this.labelCatergory = new System.Windows.Forms.Label();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.labelPublisher = new System.Windows.Forms.Label();
            this.labelSupplier = new System.Windows.Forms.Label();
            this.labelAuthor = new System.Windows.Forms.Label();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.comboBox2 = new System.Windows.Forms.ComboBox();
            this.comboBox3 = new System.Windows.Forms.ComboBox();
            this.comboBox4 = new System.Windows.Forms.ComboBox();
            this.buttonAddBook = new System.Windows.Forms.Button();
            this.buttonBback = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // labelBookName
            // 
            this.labelBookName.AutoSize = true;
            this.labelBookName.Location = new System.Drawing.Point(158, 77);
            this.labelBookName.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.labelBookName.Name = "labelBookName";
            this.labelBookName.Size = new System.Drawing.Size(63, 13);
            this.labelBookName.TabIndex = 0;
            this.labelBookName.Text = "Book Name";
            this.labelBookName.Click += new System.EventHandler(this.label1_Click);
            // 
            // labelCatergory
            // 
            this.labelCatergory.AutoSize = true;
            this.labelCatergory.Location = new System.Drawing.Point(167, 107);
            this.labelCatergory.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.labelCatergory.Name = "labelCatergory";
            this.labelCatergory.Size = new System.Drawing.Size(52, 13);
            this.labelCatergory.TabIndex = 1;
            this.labelCatergory.Text = "Catergory";
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(231, 77);
            this.textBox1.Margin = new System.Windows.Forms.Padding(2);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(114, 20);
            this.textBox1.TabIndex = 2;
            // 
            // labelPublisher
            // 
            this.labelPublisher.AutoSize = true;
            this.labelPublisher.Location = new System.Drawing.Point(170, 137);
            this.labelPublisher.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.labelPublisher.Name = "labelPublisher";
            this.labelPublisher.Size = new System.Drawing.Size(50, 13);
            this.labelPublisher.TabIndex = 4;
            this.labelPublisher.Text = "Publisher";
            // 
            // labelSupplier
            // 
            this.labelSupplier.AutoSize = true;
            this.labelSupplier.Location = new System.Drawing.Point(175, 162);
            this.labelSupplier.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.labelSupplier.Name = "labelSupplier";
            this.labelSupplier.Size = new System.Drawing.Size(45, 13);
            this.labelSupplier.TabIndex = 5;
            this.labelSupplier.Text = "Supplier";
            // 
            // labelAuthor
            // 
            this.labelAuthor.AutoSize = true;
            this.labelAuthor.Location = new System.Drawing.Point(181, 188);
            this.labelAuthor.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.labelAuthor.Name = "labelAuthor";
            this.labelAuthor.Size = new System.Drawing.Size(38, 13);
            this.labelAuthor.TabIndex = 6;
            this.labelAuthor.Text = "Author";
            // 
            // comboBox1
            // 
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Location = new System.Drawing.Point(231, 135);
            this.comboBox1.Margin = new System.Windows.Forms.Padding(2);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(114, 21);
            this.comboBox1.TabIndex = 7;
            this.comboBox1.SelectedIndexChanged += new System.EventHandler(this.comboBox1_SelectedIndexChanged);
            // 
            // comboBox2
            // 
            this.comboBox2.FormattingEnabled = true;
            this.comboBox2.Location = new System.Drawing.Point(231, 161);
            this.comboBox2.Margin = new System.Windows.Forms.Padding(2);
            this.comboBox2.Name = "comboBox2";
            this.comboBox2.Size = new System.Drawing.Size(114, 21);
            this.comboBox2.TabIndex = 8;
            // 
            // comboBox3
            // 
            this.comboBox3.FormattingEnabled = true;
            this.comboBox3.Location = new System.Drawing.Point(231, 186);
            this.comboBox3.Margin = new System.Windows.Forms.Padding(2);
            this.comboBox3.Name = "comboBox3";
            this.comboBox3.Size = new System.Drawing.Size(114, 21);
            this.comboBox3.TabIndex = 9;
            // 
            // comboBox4
            // 
            this.comboBox4.FormattingEnabled = true;
            this.comboBox4.Location = new System.Drawing.Point(231, 105);
            this.comboBox4.Margin = new System.Windows.Forms.Padding(2);
            this.comboBox4.Name = "comboBox4";
            this.comboBox4.Size = new System.Drawing.Size(114, 21);
            this.comboBox4.TabIndex = 10;
            this.comboBox4.SelectedIndexChanged += new System.EventHandler(this.comboBox4_SelectedIndexChanged);
            // 
            // buttonAddBook
            // 
            this.buttonAddBook.Location = new System.Drawing.Point(153, 232);
            this.buttonAddBook.Margin = new System.Windows.Forms.Padding(2);
            this.buttonAddBook.Name = "buttonAddBook";
            this.buttonAddBook.Size = new System.Drawing.Size(72, 27);
            this.buttonAddBook.TabIndex = 12;
            this.buttonAddBook.Text = "Add";
            this.buttonAddBook.UseVisualStyleBackColor = true;
            this.buttonAddBook.Click += new System.EventHandler(this.buttonAddBook_Click);
            // 
            // buttonBback
            // 
            this.buttonBback.Location = new System.Drawing.Point(306, 232);
            this.buttonBback.Margin = new System.Windows.Forms.Padding(2);
            this.buttonBback.Name = "buttonBback";
            this.buttonBback.Size = new System.Drawing.Size(72, 27);
            this.buttonBback.TabIndex = 13;
            this.buttonBback.Text = "Back";
            this.buttonBback.UseVisualStyleBackColor = true;
            this.buttonBback.Click += new System.EventHandler(this.buttonBback_Click);
            // 
            // Add_Book
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(533, 292);
            this.Controls.Add(this.buttonBback);
            this.Controls.Add(this.buttonAddBook);
            this.Controls.Add(this.comboBox4);
            this.Controls.Add(this.comboBox3);
            this.Controls.Add(this.comboBox2);
            this.Controls.Add(this.comboBox1);
            this.Controls.Add(this.labelAuthor);
            this.Controls.Add(this.labelSupplier);
            this.Controls.Add(this.labelPublisher);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.labelCatergory);
            this.Controls.Add(this.labelBookName);
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "Add_Book";
            this.Text = "Add_Book";
            this.Load += new System.EventHandler(this.Add_Book_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label labelBookName;
        private System.Windows.Forms.Label labelCatergory;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Label labelPublisher;
        private System.Windows.Forms.Label labelSupplier;
        private System.Windows.Forms.Label labelAuthor;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.ComboBox comboBox2;
        private System.Windows.Forms.ComboBox comboBox3;
        private System.Windows.Forms.ComboBox comboBox4;
        private System.Windows.Forms.Button buttonAddBook;
        private System.Windows.Forms.Button buttonBback;
    }
}