﻿namespace COMPX323
{
    partial class Add_Publisher
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.labelPname = new System.Windows.Forms.Label();
            this.labelPphone = new System.Windows.Forms.Label();
            this.labelPemail = new System.Windows.Forms.Label();
            this.labelPstnum = new System.Windows.Forms.Label();
            this.labelPstname = new System.Windows.Forms.Label();
            this.labelPcity = new System.Windows.Forms.Label();
            this.textBoxPname = new System.Windows.Forms.TextBox();
            this.textBoxPphone = new System.Windows.Forms.TextBox();
            this.textBoxPemail = new System.Windows.Forms.TextBox();
            this.textBoxpstnum = new System.Windows.Forms.TextBox();
            this.textBoxPstname = new System.Windows.Forms.TextBox();
            this.textBoxPcity = new System.Windows.Forms.TextBox();
            this.buttonPadd = new System.Windows.Forms.Button();
            this.buttonPback = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // labelPname
            // 
            this.labelPname.AutoSize = true;
            this.labelPname.Location = new System.Drawing.Point(137, 90);
            this.labelPname.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.labelPname.Name = "labelPname";
            this.labelPname.Size = new System.Drawing.Size(35, 13);
            this.labelPname.TabIndex = 0;
            this.labelPname.Text = "Name";
            // 
            // labelPphone
            // 
            this.labelPphone.AutoSize = true;
            this.labelPphone.Location = new System.Drawing.Point(135, 110);
            this.labelPphone.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.labelPphone.Name = "labelPphone";
            this.labelPphone.Size = new System.Drawing.Size(38, 13);
            this.labelPphone.TabIndex = 1;
            this.labelPphone.Text = "Phone";
            // 
            // labelPemail
            // 
            this.labelPemail.AutoSize = true;
            this.labelPemail.Location = new System.Drawing.Point(139, 131);
            this.labelPemail.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.labelPemail.Name = "labelPemail";
            this.labelPemail.Size = new System.Drawing.Size(32, 13);
            this.labelPemail.TabIndex = 2;
            this.labelPemail.Text = "Email";
            // 
            // labelPstnum
            // 
            this.labelPstnum.AutoSize = true;
            this.labelPstnum.Location = new System.Drawing.Point(96, 150);
            this.labelPstnum.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.labelPstnum.Name = "labelPstnum";
            this.labelPstnum.Size = new System.Drawing.Size(75, 13);
            this.labelPstnum.TabIndex = 3;
            this.labelPstnum.Text = "Street Number";
            // 
            // labelPstname
            // 
            this.labelPstname.AutoSize = true;
            this.labelPstname.Location = new System.Drawing.Point(105, 172);
            this.labelPstname.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.labelPstname.Name = "labelPstname";
            this.labelPstname.Size = new System.Drawing.Size(66, 13);
            this.labelPstname.TabIndex = 4;
            this.labelPstname.Text = "Street Name";
            // 
            // labelPcity
            // 
            this.labelPcity.AutoSize = true;
            this.labelPcity.Location = new System.Drawing.Point(148, 190);
            this.labelPcity.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.labelPcity.Name = "labelPcity";
            this.labelPcity.Size = new System.Drawing.Size(24, 13);
            this.labelPcity.TabIndex = 5;
            this.labelPcity.Text = "City";
            // 
            // textBoxPname
            // 
            this.textBoxPname.Location = new System.Drawing.Point(176, 90);
            this.textBoxPname.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.textBoxPname.Name = "textBoxPname";
            this.textBoxPname.Size = new System.Drawing.Size(107, 20);
            this.textBoxPname.TabIndex = 6;
            // 
            // textBoxPphone
            // 
            this.textBoxPphone.Location = new System.Drawing.Point(176, 110);
            this.textBoxPphone.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.textBoxPphone.Name = "textBoxPphone";
            this.textBoxPphone.Size = new System.Drawing.Size(107, 20);
            this.textBoxPphone.TabIndex = 7;
            // 
            // textBoxPemail
            // 
            this.textBoxPemail.Location = new System.Drawing.Point(176, 131);
            this.textBoxPemail.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.textBoxPemail.Name = "textBoxPemail";
            this.textBoxPemail.Size = new System.Drawing.Size(107, 20);
            this.textBoxPemail.TabIndex = 8;
            // 
            // textBoxpstnum
            // 
            this.textBoxpstnum.Location = new System.Drawing.Point(176, 152);
            this.textBoxpstnum.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.textBoxpstnum.Name = "textBoxpstnum";
            this.textBoxpstnum.Size = new System.Drawing.Size(107, 20);
            this.textBoxpstnum.TabIndex = 9;
            // 
            // textBoxPstname
            // 
            this.textBoxPstname.Location = new System.Drawing.Point(176, 173);
            this.textBoxPstname.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.textBoxPstname.Name = "textBoxPstname";
            this.textBoxPstname.Size = new System.Drawing.Size(107, 20);
            this.textBoxPstname.TabIndex = 10;
            // 
            // textBoxPcity
            // 
            this.textBoxPcity.Location = new System.Drawing.Point(175, 194);
            this.textBoxPcity.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.textBoxPcity.Name = "textBoxPcity";
            this.textBoxPcity.Size = new System.Drawing.Size(108, 20);
            this.textBoxPcity.TabIndex = 11;
            // 
            // buttonPadd
            // 
            this.buttonPadd.Location = new System.Drawing.Point(90, 259);
            this.buttonPadd.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.buttonPadd.Name = "buttonPadd";
            this.buttonPadd.Size = new System.Drawing.Size(81, 36);
            this.buttonPadd.TabIndex = 12;
            this.buttonPadd.Text = "Add";
            this.buttonPadd.UseVisualStyleBackColor = true;
            this.buttonPadd.Click += new System.EventHandler(this.buttonPadd_Click);
            // 
            // buttonPback
            // 
            this.buttonPback.Location = new System.Drawing.Point(243, 259);
            this.buttonPback.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.buttonPback.Name = "buttonPback";
            this.buttonPback.Size = new System.Drawing.Size(81, 36);
            this.buttonPback.TabIndex = 13;
            this.buttonPback.Text = "Back";
            this.buttonPback.UseVisualStyleBackColor = true;
            this.buttonPback.Click += new System.EventHandler(this.buttonPback_Click);
            // 
            // Add_Publisher
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(413, 366);
            this.Controls.Add(this.buttonPback);
            this.Controls.Add(this.buttonPadd);
            this.Controls.Add(this.textBoxPcity);
            this.Controls.Add(this.textBoxPstname);
            this.Controls.Add(this.textBoxpstnum);
            this.Controls.Add(this.textBoxPemail);
            this.Controls.Add(this.textBoxPphone);
            this.Controls.Add(this.textBoxPname);
            this.Controls.Add(this.labelPcity);
            this.Controls.Add(this.labelPstname);
            this.Controls.Add(this.labelPstnum);
            this.Controls.Add(this.labelPemail);
            this.Controls.Add(this.labelPphone);
            this.Controls.Add(this.labelPname);
            this.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.Name = "Add_Publisher";
            this.Text = "Add_Publisher";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label labelPname;
        private System.Windows.Forms.Label labelPphone;
        private System.Windows.Forms.Label labelPemail;
        private System.Windows.Forms.Label labelPstnum;
        private System.Windows.Forms.Label labelPstname;
        private System.Windows.Forms.Label labelPcity;
        private System.Windows.Forms.TextBox textBoxPname;
        private System.Windows.Forms.TextBox textBoxPphone;
        private System.Windows.Forms.TextBox textBoxPemail;
        private System.Windows.Forms.TextBox textBoxpstnum;
        private System.Windows.Forms.TextBox textBoxPstname;
        private System.Windows.Forms.TextBox textBoxPcity;
        private System.Windows.Forms.Button buttonPadd;
        private System.Windows.Forms.Button buttonPback;
    }
}