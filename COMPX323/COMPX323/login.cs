﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Oracle.ManagedDataAccess.Client;

namespace COMPX323
{
    public partial class login : Form
    {
        public login()
        {
            InitializeComponent();
            passwordTextBox.PasswordChar = '*';

      
        }

        private bool valid_login(string user, string password)
        {
            string oradb = "Data Source=oracle.cms.waikato.ac.nz:1521/teaching.cms.waikato.ac.nz;;User Id=COMPX323_08;Password=Hsp369G2FU;";
            OracleConnection conn = new OracleConnection(oradb);  // C#
            conn.Open();
            OracleCommand cmd = new OracleCommand();
            cmd.Connection = conn;
            cmd.CommandText = "select * from staff where username= '" + usernameTextBox.Text + "' and password= '" + passwordTextBox.Text + "'";
            cmd.CommandType = CommandType.Text;
            OracleDataReader dr = cmd.ExecuteReader();
            if(dr.Read())
            {
                conn.Dispose();
                return true;
            }
            else
            {
                conn.Dispose();
                MessageBox.Show("Please enter correct username and password");
                return false;
            }

            
        }


        private void login_Load(object sender, EventArgs e)
        {

        }

        private void LoginButton_Click(object sender, EventArgs e)
        {

            try
            {

                string username = usernameTextBox.Text;
                string password = passwordTextBox.Text;

                if (username == "" || password == "")
                {
                    MessageBox.Show("Please enter correct username and password");
                    return;
                }
                bool check = valid_login(username, password);

                if (check)
                {
                    Hide();
                    MainLibrary Main = new MainLibrary();
                    Main.ShowDialog();


                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
    }
}
