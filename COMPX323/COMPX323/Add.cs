﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace COMPX323
{
    public partial class Add : Form
    {
        public Add()
        {
            InitializeComponent();
        }

        private void Add_Load(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            Hide();
            Add_Book addbook = new Add_Book();
            addbook.ShowDialog();
        }

        private void buttonAddMember_Click(object sender, EventArgs e)
        {
            Hide();
            Add_Member addmem = new Add_Member();
            addmem.ShowDialog();
        }

        private void buttonAddAuthor_Click(object sender, EventArgs e)
        {
            Hide();
            Add_Author addauth = new Add_Author();
            addauth.ShowDialog();
        }

        private void buttonAddPublisher_Click(object sender, EventArgs e)
        {
            Hide();
            Add_Publisher addpub = new Add_Publisher();
            addpub.ShowDialog();
        }

        private void buttonAddSupplier_Click(object sender, EventArgs e)
        {
            Hide();
            Add_Supplier addsup = new Add_Supplier();
            addsup.ShowDialog();
        }

        private void buttonaddBack_Click(object sender, EventArgs e)
        {
            Hide();
            MainLibrary main = new MainLibrary();
            main.ShowDialog();
        }

        private void button1_Click_1(object sender, EventArgs e)
        {
  
        }
    }   
}
