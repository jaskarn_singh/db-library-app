﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Oracle.ManagedDataAccess.Client;
using MongoDB.Bson;
using MongoDB.Driver;
using MongoDB.Driver.Core;



namespace COMPX323
{
    public partial class MainLibrary : Form
    {
        public MainLibrary()
        {
            InitializeComponent();
        }

        private void listView1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
        private void listView1_DoubleClick(object sender, EventArgs e)
        {
            Edit Edit = new Edit();
            Edit.ShowDialog();
        }
        private void buttonAdd_Click(object sender, EventArgs e)
        {
            Hide();
            Add add = new Add();
            add.ShowDialog();
            
        }

        private void buttonRemove_Click(object sender, EventArgs e)
        {
            DialogResult result = MessageBox.Show("Are you sure you want to delete this book?", "Confirmation", MessageBoxButtons.YesNoCancel);
            if (result == DialogResult.Yes)
            {
                foreach (int i in listView1.SelectedIndices)
                {

                    string test = listView1.Items[i].Text;
                    listView1.Items.Remove(listView1.Items[i]);
                    string oradb = "Data Source=oracle.cms.waikato.ac.nz:1521/teaching.cms.waikato.ac.nz;;User Id=COMPX323_08;Password=Hsp369G2FU;";
                    OracleConnection conn = new OracleConnection(oradb);  // C#
                    conn.Open();
                    OracleCommand cmd = new OracleCommand();
                    cmd.Connection = conn;
                    cmd.CommandText = $"delete from written where book_id = {test}";
                    cmd.ExecuteNonQuery();
                    cmd.CommandText = $"delete from published where book_id = {test}";
                    cmd.ExecuteNonQuery();
                    cmd.CommandText = $"delete from supplied where book_id = {test}";
                    cmd.ExecuteNonQuery();
                    cmd.CommandText = $"delete from books where book_id = {test}";
                    cmd.ExecuteNonQuery();

                    conn.Dispose();
                }
            }
            else if (result == DialogResult.No)
            {

            }
           
        }

        private void buttonIssue_Click(object sender, EventArgs e)
        {
            Hide();
            Issue issue = new Issue();
            issue.ShowDialog();
        }

        private void MainLibrary_Load(object sender, EventArgs e)
        {
            string oradb = "Data Source=oracle.cms.waikato.ac.nz:1521/teaching.cms.waikato.ac.nz;;User Id=COMPX323_08;Password=Hsp369G2FU;";
            OracleConnection conn = new OracleConnection(oradb);  // C#
            conn.Open();
            OracleCommand cmd = new OracleCommand();
            cmd.Connection = conn;
            cmd.CommandText = "select bo.book_id, bo.bookname, bo.bookcategory, sup.name, pub.name, a.name from books bo join supplied s on bo.book_id = s.book_id join published p on p.book_id = bo.book_id join written w on w.book_id = bo.book_id join supplier sup on s.sup_id = sup.sup_id join publisher pub on p.pub_id = pub.pub_id join author a on w.author_id = a.author_id";


            cmd.CommandType = CommandType.Text;
            OracleDataReader dr = cmd.ExecuteReader();
            while (dr.Read())
            {
                ListViewItem lv = new ListViewItem(dr[0].ToString());
                lv.SubItems.Add(dr[1].ToString());
                lv.SubItems.Add(dr[2].ToString());
                lv.SubItems.Add(dr[3].ToString());
                lv.SubItems.Add(dr[4].ToString());
                lv.SubItems.Add(dr[5].ToString());
                listView1.Items.Add(lv);
            }
            conn.Dispose();
        }

        private void buttonLogout_Click(object sender, EventArgs e)
        {
            Hide();
            login login = new login();
            login.ShowDialog();
        }
    }
}
