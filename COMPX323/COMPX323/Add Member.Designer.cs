﻿namespace COMPX323
{
    partial class Add_Member
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.labelMFname = new System.Windows.Forms.Label();
            this.labelMLname = new System.Windows.Forms.Label();
            this.labelMPhone = new System.Windows.Forms.Label();
            this.labelMemail = new System.Windows.Forms.Label();
            this.labelMstnum = new System.Windows.Forms.Label();
            this.labelStname = new System.Windows.Forms.Label();
            this.labelMcity = new System.Windows.Forms.Label();
            this.textBoxMFname = new System.Windows.Forms.TextBox();
            this.textBoxMLname = new System.Windows.Forms.TextBox();
            this.textBoxMPhone = new System.Windows.Forms.TextBox();
            this.textBoxMEmail = new System.Windows.Forms.TextBox();
            this.textBoxMstnum = new System.Windows.Forms.TextBox();
            this.textBoxStname = new System.Windows.Forms.TextBox();
            this.textBoxMcity = new System.Windows.Forms.TextBox();
            this.buttonAddMember = new System.Windows.Forms.Button();
            this.buttonMback = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // labelMFname
            // 
            this.labelMFname.AutoSize = true;
            this.labelMFname.Location = new System.Drawing.Point(119, 79);
            this.labelMFname.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.labelMFname.Name = "labelMFname";
            this.labelMFname.Size = new System.Drawing.Size(57, 13);
            this.labelMFname.TabIndex = 0;
            this.labelMFname.Text = "First Name";
            // 
            // labelMLname
            // 
            this.labelMLname.AutoSize = true;
            this.labelMLname.Location = new System.Drawing.Point(119, 101);
            this.labelMLname.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.labelMLname.Name = "labelMLname";
            this.labelMLname.Size = new System.Drawing.Size(58, 13);
            this.labelMLname.TabIndex = 1;
            this.labelMLname.Text = "Last Name";
            // 
            // labelMPhone
            // 
            this.labelMPhone.AutoSize = true;
            this.labelMPhone.Location = new System.Drawing.Point(140, 122);
            this.labelMPhone.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.labelMPhone.Name = "labelMPhone";
            this.labelMPhone.Size = new System.Drawing.Size(38, 13);
            this.labelMPhone.TabIndex = 2;
            this.labelMPhone.Text = "Phone";
            // 
            // labelMemail
            // 
            this.labelMemail.AutoSize = true;
            this.labelMemail.Location = new System.Drawing.Point(145, 142);
            this.labelMemail.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.labelMemail.Name = "labelMemail";
            this.labelMemail.Size = new System.Drawing.Size(32, 13);
            this.labelMemail.TabIndex = 3;
            this.labelMemail.Text = "Email";
            // 
            // labelMstnum
            // 
            this.labelMstnum.AutoSize = true;
            this.labelMstnum.Location = new System.Drawing.Point(101, 164);
            this.labelMstnum.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.labelMstnum.Name = "labelMstnum";
            this.labelMstnum.Size = new System.Drawing.Size(75, 13);
            this.labelMstnum.TabIndex = 4;
            this.labelMstnum.Text = "Street Number";
            // 
            // labelStname
            // 
            this.labelStname.AutoSize = true;
            this.labelStname.Location = new System.Drawing.Point(111, 185);
            this.labelStname.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.labelStname.Name = "labelStname";
            this.labelStname.Size = new System.Drawing.Size(66, 13);
            this.labelStname.TabIndex = 5;
            this.labelStname.Text = "Street Name";
            // 
            // labelMcity
            // 
            this.labelMcity.AutoSize = true;
            this.labelMcity.Location = new System.Drawing.Point(153, 205);
            this.labelMcity.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.labelMcity.Name = "labelMcity";
            this.labelMcity.Size = new System.Drawing.Size(24, 13);
            this.labelMcity.TabIndex = 6;
            this.labelMcity.Text = "City";
            // 
            // textBoxMFname
            // 
            this.textBoxMFname.Location = new System.Drawing.Point(181, 77);
            this.textBoxMFname.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.textBoxMFname.Name = "textBoxMFname";
            this.textBoxMFname.Size = new System.Drawing.Size(126, 20);
            this.textBoxMFname.TabIndex = 7;
            // 
            // textBoxMLname
            // 
            this.textBoxMLname.Location = new System.Drawing.Point(181, 99);
            this.textBoxMLname.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.textBoxMLname.Name = "textBoxMLname";
            this.textBoxMLname.Size = new System.Drawing.Size(126, 20);
            this.textBoxMLname.TabIndex = 8;
            // 
            // textBoxMPhone
            // 
            this.textBoxMPhone.Location = new System.Drawing.Point(181, 120);
            this.textBoxMPhone.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.textBoxMPhone.Name = "textBoxMPhone";
            this.textBoxMPhone.Size = new System.Drawing.Size(126, 20);
            this.textBoxMPhone.TabIndex = 9;
            // 
            // textBoxMEmail
            // 
            this.textBoxMEmail.Location = new System.Drawing.Point(181, 140);
            this.textBoxMEmail.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.textBoxMEmail.Name = "textBoxMEmail";
            this.textBoxMEmail.Size = new System.Drawing.Size(126, 20);
            this.textBoxMEmail.TabIndex = 10;
            // 
            // textBoxMstnum
            // 
            this.textBoxMstnum.Location = new System.Drawing.Point(181, 162);
            this.textBoxMstnum.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.textBoxMstnum.Name = "textBoxMstnum";
            this.textBoxMstnum.Size = new System.Drawing.Size(126, 20);
            this.textBoxMstnum.TabIndex = 11;
            // 
            // textBoxStname
            // 
            this.textBoxStname.Location = new System.Drawing.Point(181, 183);
            this.textBoxStname.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.textBoxStname.Name = "textBoxStname";
            this.textBoxStname.Size = new System.Drawing.Size(126, 20);
            this.textBoxStname.TabIndex = 12;
            // 
            // textBoxMcity
            // 
            this.textBoxMcity.Location = new System.Drawing.Point(181, 203);
            this.textBoxMcity.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.textBoxMcity.Name = "textBoxMcity";
            this.textBoxMcity.Size = new System.Drawing.Size(126, 20);
            this.textBoxMcity.TabIndex = 13;
            // 
            // buttonAddMember
            // 
            this.buttonAddMember.Location = new System.Drawing.Point(107, 240);
            this.buttonAddMember.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.buttonAddMember.Name = "buttonAddMember";
            this.buttonAddMember.Size = new System.Drawing.Size(70, 31);
            this.buttonAddMember.TabIndex = 14;
            this.buttonAddMember.Text = "Add";
            this.buttonAddMember.UseVisualStyleBackColor = true;
            this.buttonAddMember.Click += new System.EventHandler(this.buttonAddMember_Click);
            // 
            // buttonMback
            // 
            this.buttonMback.Location = new System.Drawing.Point(257, 240);
            this.buttonMback.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.buttonMback.Name = "buttonMback";
            this.buttonMback.Size = new System.Drawing.Size(70, 31);
            this.buttonMback.TabIndex = 15;
            this.buttonMback.Text = "Back";
            this.buttonMback.UseVisualStyleBackColor = true;
            this.buttonMback.Click += new System.EventHandler(this.buttonMback_Click);
            // 
            // Add_Member
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(429, 311);
            this.Controls.Add(this.buttonMback);
            this.Controls.Add(this.buttonAddMember);
            this.Controls.Add(this.textBoxMcity);
            this.Controls.Add(this.textBoxStname);
            this.Controls.Add(this.textBoxMstnum);
            this.Controls.Add(this.textBoxMEmail);
            this.Controls.Add(this.textBoxMPhone);
            this.Controls.Add(this.textBoxMLname);
            this.Controls.Add(this.textBoxMFname);
            this.Controls.Add(this.labelMcity);
            this.Controls.Add(this.labelStname);
            this.Controls.Add(this.labelMstnum);
            this.Controls.Add(this.labelMemail);
            this.Controls.Add(this.labelMPhone);
            this.Controls.Add(this.labelMLname);
            this.Controls.Add(this.labelMFname);
            this.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.Name = "Add_Member";
            this.Text = "Add_Member";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label labelMFname;
        private System.Windows.Forms.Label labelMLname;
        private System.Windows.Forms.Label labelMPhone;
        private System.Windows.Forms.Label labelMemail;
        private System.Windows.Forms.Label labelMstnum;
        private System.Windows.Forms.Label labelStname;
        private System.Windows.Forms.Label labelMcity;
        private System.Windows.Forms.TextBox textBoxMFname;
        private System.Windows.Forms.TextBox textBoxMLname;
        private System.Windows.Forms.TextBox textBoxMPhone;
        private System.Windows.Forms.TextBox textBoxMEmail;
        private System.Windows.Forms.TextBox textBoxMstnum;
        private System.Windows.Forms.TextBox textBoxStname;
        private System.Windows.Forms.TextBox textBoxMcity;
        private System.Windows.Forms.Button buttonAddMember;
        private System.Windows.Forms.Button buttonMback;
    }
}