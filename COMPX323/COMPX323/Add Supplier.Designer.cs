﻿namespace COMPX323
{
    partial class Add_Supplier
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.labelSname = new System.Windows.Forms.Label();
            this.labelSemail = new System.Windows.Forms.Label();
            this.labelSstnum = new System.Windows.Forms.Label();
            this.labelSstname = new System.Windows.Forms.Label();
            this.labelScity = new System.Windows.Forms.Label();
            this.textBoxSname = new System.Windows.Forms.TextBox();
            this.textBoxSemail = new System.Windows.Forms.TextBox();
            this.textBoxSstnum = new System.Windows.Forms.TextBox();
            this.textBoxSstname = new System.Windows.Forms.TextBox();
            this.textBoxScity = new System.Windows.Forms.TextBox();
            this.buttonSadd = new System.Windows.Forms.Button();
            this.buttonSback = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // labelSname
            // 
            this.labelSname.AutoSize = true;
            this.labelSname.Location = new System.Drawing.Point(127, 103);
            this.labelSname.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.labelSname.Name = "labelSname";
            this.labelSname.Size = new System.Drawing.Size(35, 13);
            this.labelSname.TabIndex = 0;
            this.labelSname.Text = "Name";
            // 
            // labelSemail
            // 
            this.labelSemail.AutoSize = true;
            this.labelSemail.Location = new System.Drawing.Point(129, 124);
            this.labelSemail.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.labelSemail.Name = "labelSemail";
            this.labelSemail.Size = new System.Drawing.Size(32, 13);
            this.labelSemail.TabIndex = 1;
            this.labelSemail.Text = "Email";
            // 
            // labelSstnum
            // 
            this.labelSstnum.AutoSize = true;
            this.labelSstnum.Location = new System.Drawing.Point(86, 145);
            this.labelSstnum.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.labelSstnum.Name = "labelSstnum";
            this.labelSstnum.Size = new System.Drawing.Size(75, 13);
            this.labelSstnum.TabIndex = 2;
            this.labelSstnum.Text = "Street Number";
            // 
            // labelSstname
            // 
            this.labelSstname.AutoSize = true;
            this.labelSstname.Location = new System.Drawing.Point(95, 166);
            this.labelSstname.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.labelSstname.Name = "labelSstname";
            this.labelSstname.Size = new System.Drawing.Size(66, 13);
            this.labelSstname.TabIndex = 3;
            this.labelSstname.Text = "Street Name";
            // 
            // labelScity
            // 
            this.labelScity.AutoSize = true;
            this.labelScity.Location = new System.Drawing.Point(138, 187);
            this.labelScity.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.labelScity.Name = "labelScity";
            this.labelScity.Size = new System.Drawing.Size(24, 13);
            this.labelScity.TabIndex = 4;
            this.labelScity.Text = "City";
            // 
            // textBoxSname
            // 
            this.textBoxSname.Location = new System.Drawing.Point(165, 101);
            this.textBoxSname.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.textBoxSname.Name = "textBoxSname";
            this.textBoxSname.Size = new System.Drawing.Size(107, 20);
            this.textBoxSname.TabIndex = 5;
            // 
            // textBoxSemail
            // 
            this.textBoxSemail.Location = new System.Drawing.Point(165, 122);
            this.textBoxSemail.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.textBoxSemail.Name = "textBoxSemail";
            this.textBoxSemail.Size = new System.Drawing.Size(107, 20);
            this.textBoxSemail.TabIndex = 6;
            // 
            // textBoxSstnum
            // 
            this.textBoxSstnum.Location = new System.Drawing.Point(165, 143);
            this.textBoxSstnum.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.textBoxSstnum.Name = "textBoxSstnum";
            this.textBoxSstnum.Size = new System.Drawing.Size(107, 20);
            this.textBoxSstnum.TabIndex = 7;
            // 
            // textBoxSstname
            // 
            this.textBoxSstname.Location = new System.Drawing.Point(165, 164);
            this.textBoxSstname.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.textBoxSstname.Name = "textBoxSstname";
            this.textBoxSstname.Size = new System.Drawing.Size(107, 20);
            this.textBoxSstname.TabIndex = 8;
            // 
            // textBoxScity
            // 
            this.textBoxScity.Location = new System.Drawing.Point(165, 185);
            this.textBoxScity.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.textBoxScity.Name = "textBoxScity";
            this.textBoxScity.Size = new System.Drawing.Size(107, 20);
            this.textBoxScity.TabIndex = 9;
            // 
            // buttonSadd
            // 
            this.buttonSadd.Location = new System.Drawing.Point(91, 229);
            this.buttonSadd.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.buttonSadd.Name = "buttonSadd";
            this.buttonSadd.Size = new System.Drawing.Size(71, 29);
            this.buttonSadd.TabIndex = 10;
            this.buttonSadd.Text = "Add";
            this.buttonSadd.UseVisualStyleBackColor = true;
            this.buttonSadd.Click += new System.EventHandler(this.buttonSadd_Click);
            // 
            // buttonSback
            // 
            this.buttonSback.Location = new System.Drawing.Point(227, 229);
            this.buttonSback.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.buttonSback.Name = "buttonSback";
            this.buttonSback.Size = new System.Drawing.Size(71, 29);
            this.buttonSback.TabIndex = 11;
            this.buttonSback.Text = "Back";
            this.buttonSback.UseVisualStyleBackColor = true;
            this.buttonSback.Click += new System.EventHandler(this.buttonSback_Click);
            // 
            // Add_Supplier
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(377, 322);
            this.Controls.Add(this.buttonSback);
            this.Controls.Add(this.buttonSadd);
            this.Controls.Add(this.textBoxScity);
            this.Controls.Add(this.textBoxSstname);
            this.Controls.Add(this.textBoxSstnum);
            this.Controls.Add(this.textBoxSemail);
            this.Controls.Add(this.textBoxSname);
            this.Controls.Add(this.labelScity);
            this.Controls.Add(this.labelSstname);
            this.Controls.Add(this.labelSstnum);
            this.Controls.Add(this.labelSemail);
            this.Controls.Add(this.labelSname);
            this.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.Name = "Add_Supplier";
            this.Text = "Add_Supplier";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label labelSname;
        private System.Windows.Forms.Label labelSemail;
        private System.Windows.Forms.Label labelSstnum;
        private System.Windows.Forms.Label labelSstname;
        private System.Windows.Forms.Label labelScity;
        private System.Windows.Forms.TextBox textBoxSname;
        private System.Windows.Forms.TextBox textBoxSemail;
        private System.Windows.Forms.TextBox textBoxSstnum;
        private System.Windows.Forms.TextBox textBoxSstname;
        private System.Windows.Forms.TextBox textBoxScity;
        private System.Windows.Forms.Button buttonSadd;
        private System.Windows.Forms.Button buttonSback;
    }
}